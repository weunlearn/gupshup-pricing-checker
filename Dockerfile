FROM golang:1.17-alpine

WORKDIR /app

COPY go.mod .
COPY go.sum .

RUN go mod download

COPY main.go .

COPY .env .

RUN go build -o /main

CMD ["/main"]