package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/joho/godotenv"
	"github.com/robfig/cron"
	log "github.com/sirupsen/logrus"
	"github.com/slack-go/slack"
	"net/http"
	"os"
	"strconv"
	"time"
)

func checkEnvVariable(envVar string) {
	_, ok := os.LookupEnv(envVar)
	if !ok {
		log.Fatalln("Environment variable %s not set. Program will now exit.", envVar)
		os.Exit(3)
	}
}
func init() {
	log.SetLevel(log.InfoLevel)
	log.SetFormatter(&log.TextFormatter{FullTimestamp: true})
	err := godotenv.Load(".env")
	if err != nil {
		log.Info(err)
	}
	envVars := []string{"MIN_BALANCE", "SLACK_AUTH_TOKEN", "SLACK_CHANNEL_ID", "GUPSHUP_API_KEY"}
	for i := 0; i < 4; i++ {
		checkEnvVariable(envVars[i])
	}
}
func checkMinimumBalance(balance float64) bool {
	balanceAlert := false
	minBalance, err := strconv.ParseFloat(os.Getenv("MIN_BALANCE"), 32)
	if err != nil {
		log.Fatalln(err)
	}
	if balance <= minBalance {
		balanceAlert = true
	}
	return balanceAlert

}
func sendSlack(balance float64, message string) {
	token := os.Getenv("SLACK_AUTH_TOKEN")
	channelID := os.Getenv("SLACK_CHANNEL_ID")
	api := slack.New(token)
	message = message + fmt.Sprint(balance)
	_, timestamp, err := api.PostMessage(
		channelID,
		// uncomment the item below to add a extra Header to the message, try it out :)
		slack.MsgOptionText(message, true),
	)
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Printf("Message sent at %s", timestamp)
}

func balanceCheck(daily bool) {
	apiKey := os.Getenv("GUPSHUP_API_KEY")
	gupshupUrl := "https://api.gupshup.io/sm/api/v2/wallet/balance"
	var reqBody bytes.Buffer
	client := &http.Client{}
	log.Info("Created client...")
	req, err := http.NewRequest(
		"GET",
		gupshupUrl,
		&reqBody,
	)
	if err != nil {
		log.Fatalln(err)
	}
	req.Header.Set("apikey", apiKey)
	log.Info("Request sent...")
	resp, err := client.Do(req)
	if err != nil {
		log.Fatalln(err)
	}
	log.Info(resp)
	var result map[string]interface{}
	err = json.NewDecoder(resp.Body).Decode(&result)
	if err != nil {
		log.Fatalln(err)
	}
	bal := result["balance"].(float64)
	log.Printf("Balance: %v", bal)
	if !daily {
		alert := checkMinimumBalance(bal)
		if alert {
			message := "GUPSHUP TEST ALERT\n YOUR GUPSHUP BALANCE IS LOW <!here> . Remaining balance: "
			sendSlack(bal, message)
		} else {
			log.Info("All good")
		}
	} else {
		message := "Daily Gupshup Update\n GUPSHUP BALANCE: "
		sendSlack(bal, message)
	}
}
func main() {

	log.Info("Created cron...")
	c := cron.New()
       
	err := c.AddFunc("0 30 * * * *", func() {
		balanceCheck(false)
	})
	if err != nil {
		log.Fatalln(err)
	}
	err = c.AddFunc("0 0 * * *", func() {
		balanceCheck(true)
	})
	if err != nil {
		log.Fatalln(err)
	}
	log.Info("Start cron")
	c.Start()
	time.Sleep(2 * time.Minute)

}
