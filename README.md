# Gupshup Price Alert App
This repo contains Go code that creates a cron job to check for gupshup wallet balance every 0th and 30th minute of an hour. If the wallet balance is below minimum amount, an alert in sent to a slack channel. It also sends a daily update on the wallet balance once a day.

## Installation
Clone the repo and copy the .env.example file to .env

Update the required tokens. 

Run 

```docker build -t [IMAGE_NAME] .```
To directly use the docker image
```docker run [IMAGE_NAME]```
Alternatively, you can push the newly build docker image into a repository and use it in your workflows as required.

